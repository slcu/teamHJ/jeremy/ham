########################
### Test script
########################

import sys
from matplotlib import pyplot as pp
from routines import *
import numpy as np
from scipy.optimize import minimize
from time import gmtime, strftime
import scipy
from matplotlib import pyplot as pp


job = sys.argv[1]

### Reads the parameters from job file

chan = open("OPT/"+job,"r")
lines = chan.readlines()
chan.close()
params = []
for l in lines:
    s=l[:-1].split(" ")
    while "" in s: s.remove("")
    params.append([float(e) for e in s])

print params

bWparams=params[0]
Wparams=params[1]
Cparams=params[2]
cparams=params[3]

vW = 2; ncyt = 8; nahk = 4; ncW = 2
vH = 1; nlh = 4

#### Builds the various elements required for computation and plotting
#dimension of the grid and radius of the meristem
dim = 50
rad = int(sys.argv[2])
#N: neighborhood matrix
#L: L1 matrix
#S: Sink matrix
#to_flat, to_square: conversion methods between matrices used for display/building and vectors used for computations
#dim2: length of the "flat" meristem vectors (all the computation vectors) = nb of cells in the meristem
D,N,L,S,dim2,to_flat,to_square = build_DNLS(rad,dim)
#neighborhood, sink and L1 vectors
fN, fS, fL= flatten(N,dim,dim2,to_square), flatten(S,dim,dim2,to_square), flatten(L,dim,dim2,to_square)

#optimisation targets for WUS, CLV3 and HAM
WT = np.zeros((dim,dim)); CT = np.zeros((dim,dim)); HT = np.zeros((dim,dim));
for i in range(dim):
    for j in range(dim):
        if j**2 + (i-15)**2 <= 5**2: WT[i,j] = 1
        if j**2 + (i-30)**2 <= 10**2: CT[i,j] = 1
        if j**2 + (i+10)**2 <= 30**2: HT[i,j] = 1
fWT = flatten(WT,dim,dim2,to_square)
fCT = flatten(CT,dim,dim2,to_square)
fHT = flatten(HT,dim,dim2,to_square)

#coordinates for sparse matrices
rows, cols, data, dia = build_topo(dim,to_square,to_flat,D,fN,fS)
n = 4
levels = np.arange(0,2,2/100.)

plot_list = []

# Shea-Ackers for CLV3 expression
def shea(W,D,kw,kd):
    return (kw*W) / (1 + kw*W + kd*D)

# derivatives of the monomers/dimers
def f(a,b,d,A,B,pa,ga,da,pb,gb,db,gd,dd,forw,back):
    return np.concatenate(
        (another_deriv_x(pa*A+back*d,da,ga+forw*b,a,rows,cols,dia),
         another_deriv_x(pb*B+back*d,db,gb+forw*a,b,rows,cols,dia),
         another_deriv_x(forw*a*b,dd,gd+back,d,rows,cols,dia)))

# jacobian of the monomers/dimers
def j(a,b,d,pa,ga,da,pb,gb,db,gd,dd,forw,back):
    dia_a = -ga -da*dia -forw*b
    dia_b = -gb -db*dia -forw*a
    dia_d = -gd -dd*dia -back
    
    dia_ab = -forw*a
    dia_ad = np.full(dim2,back)

    dia_ba = -forw*b
    dia_bd = np.full(dim2,back)

    dia_da = forw*b
    dia_db = forw*a

    dia_rowsncols = np.arange(dim2)
    diff_data = -data

    ldata = np.concatenate((
        dia_a,#dia_a
        dia_b,#dia_b
        dia_d,#dia_d
        diff_data*da,#neigh_a
        diff_data*db,#neigh_b
        diff_data*dd,#neigh_d
        dia_ab,
        dia_ad,
        dia_ba,
        dia_bd,
        dia_da,
        dia_db))

    lrows = np.concatenate((
        dia_rowsncols,#dia_a
        dia_rowsncols+dim2,#dia_b
        dia_rowsncols+dim2*2,#dia_d
        rows,#neigh_a
        rows+dim2,#neigh_b
        rows+dim2*2,#neigh_d
        dia_rowsncols,#dia_ab
        dia_rowsncols,#dia_ad
        dia_rowsncols+dim2,#dia_ba
        dia_rowsncols+dim2,#dia_bd
        dia_rowsncols+dim2*2,#dia_da
        dia_rowsncols+dim2*2))#dia_db

    lcols = np.concatenate((
        dia_rowsncols,#dia_a
        dia_rowsncols+dim2,#dia_b
        dia_rowsncols+dim2*2,#dia_d
        cols,#neigh_a
        cols+dim2,#neigh_b
        cols+dim2*2,#neigh_d
        dia_rowsncols+dim2,#dia_ab
        dia_rowsncols+dim2*2,#dia_ad
        dia_rowsncols,#dia_ba
        dia_rowsncols+dim2*2,#dia_bd
        dia_rowsncols,#dia_da
        dia_rowsncols+dim2))#dia_db

    M = sp.coo_matrix((ldata,(lrows,lcols)),shape=(dim2*3,dim2*3))
    return M.tocsc()

h,w,d = np.zeros(dim2),np.zeros(dim2),np.zeros(dim2)


# computes the stable state of the system
# mut=0: wild type
# mut=1: clv3 loss of function
# mut=2: ham loss of function
def stable(bWparams,Wparams,Cparams,cparams,mut=0):
    h,w,d = np.zeros(dim2),np.zeros(dim2),np.zeros(dim2)
    kcyt, kahk,pcyt,dcyt,gcyt,pahk,dahk,gahk = bWparams
    kcW, pc, dc= Wparams
    klh,plh,dlh,pw,dw,ph,dh,forw,dd,vC,kw,kd,gh, gw, gd, back = Cparams
    pc,dc,gc = cparams
   
    if mut==1: pc=0
    if mut==2: ph=0
    #stable state of:
    #cytokinin
    cyt = pdg(pcyt*fL,dcyt,gcyt,dia,rows,cols,data)
    #ahk repressor
    ahk = pdg(pahk*fL,dahk,gahk,dia,rows,cols,data)
    #L1 repressor of HAM
    lh = pdg(plh*fL,dlh,1,dia,rows,cols,data)
    #WUS
    W = vW * (cyt**ncyt / (cyt**ncyt + kcyt**ncyt)) * (kahk**nahk / (ahk**nahk + kahk**nahk))
    # lh = pdg(plh*fL,dlh,1,dia,rows,cols,data)
    # HAM
    H = vH * (klh**nlh / (lh**nlh + klh**nlh))
    
    #stablilisation of CLV3 and monomers/dimers using Newton method
    def stable_C(W):
        global h,w,d
        stable = 10000
        i_stable = 0; limit=100
        while stable>1e-10 and i_stable <= limit:
            i_stable+=1
            print ".",
            g = f(w,h,d,W,H,pw,gw,dw,ph,gh,dh,gd,dd,forw,back)
            M = j(w,h,d,pw,gw,dw,ph,gh,dh,gd,dd,forw,back)
            v = sp.linalg.spsolve(M,-g)
            x2 = np.concatenate((w,h,d))+v

            for delme in range(x2.shape[0]-1):
                if x2[delme]<0: x2[delme] = 0
        
                stable = np.sum(v**2)
                w,h,d = x2[:dim2],x2[dim2:dim2*2],x2[dim2*2:dim2*3]
        
        if i_stable >= limit:
            w,h,d =np.zeros(dim2),np.zeros(dim2),np.zeros(dim2)

        return H,lh,h,w,d

    bW=W.copy()
    H,lh,h,w,d = stable_C(bW)
    bC = vC * shea(w, d, kw, kd)
    ow,od=w.copy(),d.copy()
    step_size = .1
    diffs = 99999999999999
    i=-1
    ####
    # Algorithm for the stabilisation of the whole system
    ####
    while diffs > 10**-10:
        i+=1
        if i>200:sys.exit()
        bC = vC * shea(ow, od, kw, kd)
        c=pdg(pc*bC,dc,gc,dia,rows,cols,data)
        W = vW * (cyt**ncyt / (cyt**ncyt + kcyt**ncyt)) * (kahk**nahk / (ahk**nahk + kahk**nahk)) * (kcW**ncW / (c**ncW + kcW**ncW))*step_size + (1-step_size)*bW
        H,lh,h,w,d = stable_C(W)#,Cparams)
        C = vC * shea(w, d, kw, kd)
        print i,np.sum(bC-C), diffs
        if np.sum(bC-C) > 0:
            bW=W.copy(); ow,od=w.copy(),d.copy()
        else:
            step_size*=.1    
        w_derivs = np.sum(another_deriv_x(pw*W+back*d,dw,gw+forw*h,w,rows,cols,dia)**2)
        h_derivs = np.sum(another_deriv_x(ph*H+back*d,dh,gh+forw*w,h,rows,cols,dia)**2)
        d_derivs = np.sum(another_deriv_x(forw*w*h,dd,gd+back,d,rows,cols,dia)**2)
        c_derivs = np.sum(another_deriv_x(pc*C,dc,gc,c,rows,cols,dia)**2)
        diffs = w_derivs + h_derivs + c_derivs + d_derivs
#        print diffs

    w_derivs = np.sum(another_deriv_x(pw*W+back*d,dw,gw+forw*h,w,rows,cols,dia)**2)
    h_derivs = np.sum(another_deriv_x(ph*H+back*d,dh,gh+forw*w,h,rows,cols,dia)**2)
    d_derivs = np.sum(another_deriv_x(forw*w*h,dd,gd+back,d,rows,cols,dia)**2)
    c_derivs = np.sum(another_deriv_x(pc*C,dc,gc,c,rows,cols,dia)**2)
    
    print np.sum(w_derivs**2), np.sum(h_derivs**2), np.sum(d_derivs**2), np.sum(c_derivs**2)

    return W,C,H,w,h,d,c,cyt,ahk,lh


W,C,H,w,h,d,c,cyt,ahk,lh = stable(bWparams,Wparams,Cparams,cparams,mut=0)
w_save,h_save,d_save = w.copy(),h.copy(),d.copy()
CW,CC,CH,Cw,Ch,Cd,Cc,_,_,_ = stable(bWparams,Wparams,Cparams,cparams,mut=1)
Cw_save,Ch_save,Cd_save = Cw.copy(),Ch.copy(),Cd.copy()
HW,HC,HH,Hw,Hh,Hd,Hc,_,_,_ = stable(bWparams,Wparams,Cparams,cparams,mut=2)
Hw_save,Hh_save,Hd_save = Hw.copy(),Hh.copy(),Hd.copy()

# #####################
# ### computed data ###
# #####################

all_data=np.array([W,C,H,w_save,h_save,d_save,c,cyt,ahk,lh,CW,CC,CH,Cw_save,Ch_save,Cd_save,Cc,HW,HC,HH,Hw_save,Hh_save,Hd_save,Hc])
np.savetxt("DATA/"+str(job)+".data",all_data)

# ########################
# ### diffs and images ###
# ########################

max_Ld = np.max(d_save[np.where(fL==1)])
d_special = np.zeros(dim2)
d_special[np.where(d_save>max_Ld)]=1

plot_list.append((W,"WUS",False))
plot_list.append((C,"CLV3",False))
plot_list.append((H,"HAM",False))
plot_list.append((w_save,"wus monomer",True))
plot_list.append((c,"clv3",True))
plot_list.append((h_save,"ham monomer",True))
plot_list.append((d_save,"ham/wus dimer",True))
plot_list.append((d_special,"d pocket",True))
plot_list.append((HW,"WUS in ham (relative)",True))
plot_list.append((CW,"WUS in clavata",False))
plot_list.append((CC,"CLV3 in clavata",False))
plot_list.append((HC,"CLV3 in ham (relative)",True))


########## DISPLAY
plot_bunch(plot_list,dim,to_square)
########## SAVE
plot_bunch(plot_list,dim,to_square,save="DATA/"+str(job))


